jQuery(function($) {

	//Preloader
	var preloader = $('.preloader');
	$(window).load(function(){
		preloader.remove();
	});

	//#main-slider
	var slideHeight = $(window).height();
	$('#home-slider .item').css('height',slideHeight);

	$(window).resize(function(){'use strict',
		$('#home-slider .item').css('height',slideHeight);
	});
	
	//Scroll Menu
	$(window).on('scroll', function(){
		if( $(window).scrollTop()>slideHeight ){
			$('.main-nav').addClass('navbar-fixed-top');
		} else {
			$('.main-nav').removeClass('navbar-fixed-top');
		}
	});
	
	// Navigation Scroll
	$(window).scroll(function(event) {
		Scroll();
	});

	$('.navbar-collapse ul li a').on('click', function() {  
		$('html, body').animate({scrollTop: $(this.hash).offset().top - 5}, 1000);
		return false;
	});

	// User define function
	function Scroll() {
		var contentTop      =   [];
		var contentBottom   =   [];
		var winTop      =   $(window).scrollTop();
		var rangeTop    =   200;
		var rangeBottom =   500;
		$('.navbar-collapse').find('.scroll a').each(function(){
			contentTop.push( $( $(this).attr('href') ).offset().top);
			contentBottom.push( $( $(this).attr('href') ).offset().top + $( $(this).attr('href') ).height() );
		});
		$.each( contentTop, function(i){
			if ( winTop > contentTop[i] - rangeTop ){
				$('.navbar-collapse li.scroll')
				.removeClass('active')
				.eq(i).addClass('active');			
			}
		})
  }

  $('#tohash').on('click', function(){
		$('html, body').animate({scrollTop: $(this.hash).offset().top - 5}, 1000);
		return false;
	});
	
	//Initiat WOW JS
	new WOW().init();
	//smoothScroll
	smoothScroll.init();
	
	// Progress Bar
	$('#about-us').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
		if (visible) {
			$.each($('div.progress-bar'),function(){
				$(this).css('width', $(this).attr('aria-valuetransitiongoal')+'%');
			});
			$(this).unbind('inview');
		}
	});

	//Countdown
	$('#features').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
		if (visible) {
			$(this).find('.timer').each(function () {
				var $this = $(this);
				$({ Counter: 0 }).animate({ Counter: $this.text() }, {
					duration: 2000,
					easing: 'swing',
					step: function () {
						$this.text(Math.ceil(this.Counter));
					}
				});
			});
			$(this).unbind('inview');
		}
	});

	// Portfolio Single View
	$('#portfolio').on('click','.folio-read-more',function(event){
		event.preventDefault();
		var link = $(this).data('single_url');
		var full_url = '#portfolio-single-wrap',
		parts = full_url.split("#"),
		trgt = parts[1],
		target_top = $("#"+trgt).offset().top;

		$('html, body').animate({scrollTop:target_top}, 600);
		$('#portfolio-single').slideUp(500, function(){
			$(this).load(link,function(){
				$(this).slideDown(500);
			});
		});
	});

	// Close Portfolio Single View
	$('#portfolio-single-wrap').on('click', '.close-folio-item',function(event) {
		event.preventDefault();
		var full_url = '#portfolio',
		parts = full_url.split("#"),
		trgt = parts[1],
		target_offset = $("#"+trgt).offset(),
		target_top = target_offset.top;
		$('html, body').animate({scrollTop:target_top}, 600);
		$("#portfolio-single").slideUp(500);
	});

	// Contact form
	var form = $('#main-contact-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
		$.ajax({
			url: $(this).attr('action'),
			beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
			}
		}).done(function(data){
			form_status.html('<p class="text-success">Thank you for contact us. As early as possible  we will contact you</p>').delay(3000).fadeOut();
		});
	});

	// Slider: Auflage und Format
  const priceMap = new Map([
    ['5000-din a6', { price: '999,95', article: 'WA1-DINA6-1' }],
    ['10000-din a6', { price: '1.499,95', article: 'WA1-DINA6-2' }],
    ['15000-din a6', { price: '1.899,95', article: 'WA1-DINA6-3' }],
    ['20000-din a6', { price: '2.299,95', article: 'WA1-DINA6-4' }],
    ['25000-din a6', { price: '2.699,95', article: 'WA1-DINA6-5' }],
    ['30000-din a6', { price: '3.099,95', article: 'WA1-DINA6-6' }],
    ['5000-din lang', { price: '1.299,95', article: 'WA1-DINLang-1' }],
    ['10000-din lang', { price: '1.699,95', article: 'WA1-DINLang-2' }],
    ['15000-din lang', { price: '2.099,95', article: 'WA1-DINLang-3' }],
    ['20000-din lang', { price: '2.499,95', article: 'WA1-DINLang-4' }],
    ['25000-din lang', { price: '2.899,95', article: 'WA1-DINLang-5' }],
    ['30000-din lang', { price: '3.299,95', article: 'WA1-DINLang-6' }],
    ['5000-din a5', { price: '1.599,95', article: 'WA1-DINA5-1' }],
    ['10000-din a5', { price: '1.999,95', article: 'WA1-DINA5-2' }],
    ['15000-din a5', { price: '2.399,95', article: 'WA1-DINA5-3' }],
    ['20000-din a5', { price: '2.799,95', article: 'WA1-DINA5-4' }],
    ['25000-din a5', { price: '3.199,95', article: 'WA1-DINA5-5' }],
    ['30000-din a5', { price: '3.599,95', article: 'WA1-DINA5-6' }],
    ['5000-din a4', { price: '2.299,95', article: 'WA1-DINA4-1' }],
    ['10000-din a4', { price: '2.699,95', article: 'WA1-DINA4-2' }],
    ['15000-din a4', { price: '3.099,95', article: 'WA1-DINA4-3' }],
    ['20000-din a4', { price: '3.499,95', article: 'WA1-DINA4-4' }],
    ['25000-din a4', { price: '3.899,95', article: 'WA1-DINA4-5' }],
    ['30000-din a4', { price: '4.299,95', article: 'WA1-DINA4-6' }]
  ]);

  function getKeyFromUi() {
    const editionVal = $('#pricing-slider-edition').val();
    const formatVal = $('#pricing-slider-format').val();
    return (editionVal + '-' + formatVal).toLowerCase();
  }

  function getArticleFromUi() {
    const key = getKeyFromUi();
    return priceMap.get(key).article;
  }

  function setArticleForButton() {
    const article = getArticleFromUi();
    $('#requestOfferDynamic').data('article', article);
  }

  function setPrice() {
    const key = getKeyFromUi();
    $('#pricing-price').html(priceMap.get(key).price);
  }

  $('#pricing-slider-edition, #pricing-slider-format').ionRangeSlider({
    onStart: function(data) {
      if (data.input.context.id === 'pricing-slider-format') {
        setPrice();
        setArticleForButton();
			}
    },
    onChange: function(data) {
      setPrice();
      setArticleForButton();
    }
  });

  $('#offerAsPrivate, #offerAsCompany').change(function() {
    $('#grpOfferAsPrivate').toggle();
    $('#grpOfferAsCompany').toggle();
  });

  $('#offerContactDateTimeContainer').datetimepicker({
    locale: 'de'
  });

  $('#requestOffer').on('show.bs.modal', function(event) {
    const button = $(event.relatedTarget); // Button that triggered the modal
    const article = button.data('article'); // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    const modal = $(this);
    modal.find('#offerArticle').val(article);
  });

  $('#requestOfferSubmit').on('click', function(event) {
    const serializedData = $('#requestOfferForm').serialize();
    // alert(serializedData);
    $.getJSON("https://api.flyer-experten.com/requestoffer", serializedData, function(data) {
			if (data.errors) {
			  var errorMessage = '<ul>';
			  data.errors.forEach(function(err) {
			    errorMessage += '<li>' + err + '</li>';
			  });
			  errorMessage += '</ul>';
				$('#requestOfferMessages').show().html(errorMessage);
			} else {
        $('#requestOfferMessages').hide().html('');
        $('#requestOfferForm').hide();
        $('#requestOfferSuccess').show();
      }
    });
  });

  $('#contactRequestSubmit').on('click', function(event) {
    const serializedData = $('#contactRequestForm').serialize();
    // alert(serializedData);
    $.getJSON("https://api.flyer-experten.com/contactrequest", serializedData, function(data) {
      if (data.errors) {
        var errorMessage = '<ul>';
        data.errors.forEach(function(err) {
          errorMessage += '<li>' + err + '</li>';
        });
        errorMessage += '</ul>';
        $('#contactRequestMessages').show().html(errorMessage);
      } else {
        $('#contactRequestMessages').hide().html('');
        $('#contactRequestForm').hide();
        $('#contactRequestSuccess').show();
      }
    });
	});

  // job application
  $('#jobapplicationbirthdateContainer,#jobapplicationavailablefromContainer,#jobapplicationavailabletoContainer').datetimepicker({
    locale: 'de',
		format: 'L',
    useCurrent: false
  });
  $("#jobapplicationavailablefromContainer").on("dp.change", function(e) {
    $('#jobapplicationavailabletoContainer').data("DateTimePicker").minDate(e.date);
  });
  $("#jobapplicationavailabletoContainer").on("dp.change", function(e) {
    $('#jobapplicationavailablefromContainer').data("DateTimePicker").maxDate(e.date);
  });
  $('#jobapplicationavailableEver').on('change', function(e) {
    $('.jobapplicationavailable').hide();
  });
  $('#jobapplicationavailablePartial').on('change', function(e) {
    $('.jobapplicationavailable').show();
  });

  $('#jobApplicationSubmit').on('click', function(event) {
    const serializedData = $('#jobApplicationForm').serialize();
    $.getJSON("https://api.flyer-experten.com/jobapplication", serializedData, function(data) {
      if (data.errors) {
        var errorMessage = '<ul>';
        data.errors.forEach(function(err) {
          errorMessage += '<li>' + err + '</li>';
        });
        errorMessage += '</ul>';
        $('#jobApplicationMessages').show().html(errorMessage);
      } else {
        $('#jobApplicationMessages').hide().html('');
        $('#jobApplicationForm').hide();
        $('#jobApplicationSuccess').show();
      }
    });
  });

  // conception
  $('#conceptionAsPrivate, #conceptionAsCompany').change(function() {
    $('#grpConceptionAsPrivate').toggle();
    $('#grpConceptionAsCompany').toggle();
  });
  $('#conceptionAdvertisingStartDateContainer').datetimepicker({
    locale: 'de',
    format: 'L',
    useCurrent: false
  });
  $('#conceptionContactDateTimeContainer').datetimepicker({
    locale: 'de'
  });
  $('#requestConceptionSubmit').on('click', function(event) {
    const serializedData = $('#requestConceptionForm').serialize();
    $.getJSON("http://localhost:3000/conceptionrequest", serializedData, function(data) {
      if (data.errors) {
        var errorMessage = '<ul>';
        data.errors.forEach(function(err) {
          errorMessage += '<li>' + err + '</li>';
        });
        errorMessage += '</ul>';
        $('#requestConceptionMessages').show().html(errorMessage);
      } else {
        $('#requestConceptionMessages').hide().html('');
        $('#requestConceptionForm').hide();
        $('#requestConceptionSuccess').show();
      }
    });
  });
});

